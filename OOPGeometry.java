import java.util.*;

class OOPGeometry extends MainMenu{

	public static void main(String[]args){
		Scanner input=new Scanner(System.in);
		MainMenu mainMenu=new MainMenu();
		
		try{
			mainMenu.display();
		}catch(NoSuchElementException e){
			System.out.println();
			System.out.println("Don't Press Ctrl+Z");
		}
	}	

}



class MainMenu{	
	static Scanner input=new Scanner(System.in);	
	void display(){

		threeDimensional threeDimension=new threeDimensional();
		twoDimensional twoDimension=new twoDimensional();
		
		do{
	    	int select=-1;
	    	System.out.println("Menu");
			System.out.println("1. Bangun Datar");
			System.out.println("2. Bangun Ruang");
			System.out.println("0. Keluar");
			System.out.println();
			System.out.print("Masukan Pilihan Anda : ");
			try{
				select=input.nextInt();	    			
			}catch(InputMismatchException e){
				System.out.println("Inputan Anda Salah");
			}	    		

		 input.nextLine();			
		    	
		switch(select){
		case 0:
			System.out.println("Terima Kasih");
			break;
		case 1:
			twoDimension.display();
			break;
		case 2:   			
	        threeDimension.display();
	        break;
		default:
			System.out.println("Inputan Anda Salah");
		}
			
		if (select==0){
			break;
		}
		
		}while(true);
	}
}
class twoDimensional extends MainMenu{	 
	 static Scanner input=new Scanner(System.in);	
	 MainMenu mainMenu=new MainMenu();
	 void display(){
		  
		 
		  square square = new square();
		  rectangle rectangle = new rectangle();
		  circle circle = new circle();
		  triangle triangle = new triangle();
		  rhombus rhombus = new rhombus();
		  parallelogram parallelogram = new parallelogram();
 
			do{
		    	int select=-1;
		    	System.out.println("\n");
		    	System.out.println("Menu Bangun Datar");
				System.out.println("1. Persegi");
				System.out.println("2. Persegi Panjang");
				System.out.println("3. Lingkaran");
				System.out.println("4. segitiga");
				System.out.println("5. Belah Ketupat");
				System.out.println("6. Trapesium");
				System.out.println("99. Kembali");
				System.out.println();
					System.out.print("Masukan Pilihan Anda : ");
		    		try{
		    			select=input.nextInt();
		    			System.out.println();
		    		}catch(InputMismatchException e){
		    			System.out.println("Inputan Anda Salah");
		    		}	    		
			
				 input.nextLine();			
				    	
				switch(select){
				case 1:
					square.calculate();
					break;
				case 2:   			
		            rectangle.calculate();
		            break;
				case 3:
					circle.calculate();
					break;
				case 4:
					triangle.calculate();
					break;
				case 5:
					rhombus.calculate();
					break;
				case 6:
					parallelogram.calculate();
					break;
				case 99:
					mainMenu.display();
					break;
				default:
					System.out.println("Inputan Anda Salah");
				
				}
				
				}while(true);
	 }
}
class threeDimensional extends MainMenu{	 
	 static Scanner input=new Scanner(System.in);
	 MainMenu mainMenu=new MainMenu();
	 void display(){
		  
	  	 cube cube = new cube();
	   	 blok blok = new blok();
		 ball ball = new ball();
		 prism prism = new prism();
		 cone cone = new cone();
		 
			do{
	     	int select=-1;
	     	System.out.println("Menu Bangun Ruang");
			System.out.println("1. Kubus");
			System.out.println("2. Balok");
			System.out.println("3. Bola");
			System.out.println("4. Prisma");
			System.out.println("5. Kerucut");
			System.out.println("99. Kembali");
			System.out.println();
	 			System.out.print("Masukan Pilihan Anda : ");
		    		try{
		    			select=input.nextInt();
		    			System.out.println("----------------------------------------");
		    		}catch(InputMismatchException e){
		    			System.out.println("Inputan Anda Salah");
		    		}	    		
	 	
	 		 input.nextLine();			
	 		
	 		switch(select){
	 		case 1:
	 			cube.calculate();
	 			break;
	 		case 2:   			
	             blok.calculate();
	             break;
	 		case 3:
	 			ball.calculate();
	 			break;
	 		case 4:
	 			prism.calculate();
	 			break;
	 		case 5:
	 			cone.calculate();
	 			break;
	 		case 99:
				mainMenu.display();
				break;
	 		default:
	 			System.out.println("Inputan Anda Salah");
	 			
	 		}
	 		
	 		
	 	}while(true); 
	 }
}
 

class cubeFormula{
		double side;
		double volume(){
			return side*side*side;
		}
		double surfArea(){
			return 12*side;
		}
}
		class cube extends cubeFormula{
				static Scanner input=new Scanner(System.in);
				 void calculate(){
				double side;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Panjang Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	side=input.nextDouble();
				    	this.side=side;
				    	if (this.side <= 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Volume Kubus: %.1f\n",volume());
				    	
				    	System.out.printf("Luas Permukaan Kubus: %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan anda Salah");
				    }
				   
				    catch(IllegalArgumentException e){
				    	if(this.side<0)System.out.println("Angka tidak boleh negatif");
				    	else if(this.side==0)System.out.println("Angka tidak boleh nol");
				    	
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try Again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if ( choice.contentEquals("y") )break;
						    else if( choice.contentEquals("n") )break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || ( choice.contentEquals("n") ) == false);
				    
				    if ( choice.contentEquals("y") )continue;
				    else if( choice.contentEquals("n") )break;
					    
				    
			    }while(true);
				}
		}
	


class blokFormula{
	 double length,width,height;
	 double volume(){
		 return length*width*height;
	 }
	 double surfArea(){
		 return (2 * length * width) + (2 * length * height) + (2 * width * height);
	 }
}	
		class blok extends blokFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double length,width,height;
		        String choice;
		        do{
				    try{
				    	System.out.println("Masukan Panjang (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	length=input.nextDouble();  	
				    	this.length=length;
				    	if (length < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (length == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukan Lebar (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	width=input.nextDouble();  
				    	this.width=width;
				    	if (width < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (width == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Volume Balok: %.1f\n",volume());
			
				    	System.out.printf("Luas Permukaan Balok: %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan anda salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka tidak boleh nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
		    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
		        }while(true);
			}
		}



class ballFormula{
	double radius;
	double volume(){
		return ( (double) 4/3 ) * (double)3.14 * radius * radius * radius;
	}
	double surfArea(){
		return 4.0 * (double)3.14 * radius * radius;
	}
}
		class ball extends ballFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double radius;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Panjang jari-jari Lingkaran (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
				    	this.radius=radius;
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    		
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Volume Bola: %.2f\n",volume());
				    	
				    	System.out.printf("Luas Permukaan Bola: %.2f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka Tidak Boleh Nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n");
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class prismFormula{
	double Base,HeightPrism,HeightTriangle;
	double volume(){
		return Base*HeightPrism;
	}
	double surfArea(){
		return 2*Base*HeightTriangle;
	}
}
		class prism extends prismFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double Base,HeightPrism,HeightTriangle;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Alas Segitiga (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	Base=input.nextDouble();
				    	this.Base=Base;
				    	if (Base < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (Base == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukan Tinggi Prisma (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	HeightPrism=input.nextDouble();
				    	this.HeightPrism=HeightPrism;
				    	if (HeightPrism < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (HeightPrism == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println("Masukan Tinggi Segitiga (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	HeightTriangle=input.nextDouble();
				    	this.HeightTriangle=HeightTriangle;
				    	if (HeightTriangle < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (HeightTriangle == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Volume Prisma: %.1f\n",volume());
				    	
				    	
				    	System.out.printf("Luas Permukaan Prisma: %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka Tidak Boleh Nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n");
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}


class coneFormula{
	double phi = (double)3.14,base,line,height;
	double volume(){
		return phi*(base*base)*height*4/3;
	}
	double surfArea(){
		return phi*base*base+3.14*base*line;
	}
	
}
		class cone extends coneFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double base,line,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Alas Kerucut (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	base=input.nextDouble();  	
				    	this.base=base;
				    	if (base < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (base == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukan Tinggi Kerucut (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println("Masukan garis pelukis Kerucut (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	line=input.nextDouble();
				    	this.line=line;
				    	if (line < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (line == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Volume Kerucut: %.1f\n",volume());
				    	
				    	System.out.printf("Luas Permukaan Kerucut: %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka Tidak Boleh Nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n");
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}


class rectangleFormula{
	double width,length;
	double area(){
		return width*length;
	}
	double perimeter(){
		return (2.0 * width) + (2.0 * length);
	}

}
		class rectangle extends rectangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
			    double width,length;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Lebar Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	width=input.nextDouble();
				    	this.width=width;
				    	if (width < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (width == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukan Panjang Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	length=input.nextDouble();
				    	this.length=length;
				    	;
				    	if (length < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (length == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Luas Persegi Panjang: %.1f\n",area());
				    	
				    	System.out.printf("Keliling Persegi Panjang: %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka Tidak Boleh Nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class triangleFormula{
	double base,height;
	double area(){
		return 0.5 * base * height;
	}
	double perimeter(){
		return base+base+base;
	}
}
		class triangle extends triangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
			    double base,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Panjang Alas Segitiga (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	base=input.nextDouble();
				    	this.base=base;
				    	if (base < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (base == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukan Tinggi Segitiga (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Luas Segitiga: %.1f\n",area());
				    	
				    	System.out.printf("Keliling Segitiga: %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka tidak boleh nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class squareFormula{
	double side;
	double area(){
		return side * side ;
	}
	double perimeter(){
		return side + side + side + side;
	}
}
		class square extends squareFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double side;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Panjang Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	side=input.nextDouble();
				    	this.side=side;
				    	
				    	
				    	if (side < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (side == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Luas Persegi: %.1f\n",area());
				    	
				    	System.out.printf("Keliling Persegi: %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka tidak boleh nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class parallelogramFormula{
	double base,height;
	double area(){
		return base * height;
	}
	double perimeter(){
		return 2.0*(base+height);
	}
}
		class parallelogram extends parallelogramFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				double base,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Panjang Alas (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	base=input.nextDouble();
				    	this.base=base;
					    	if (base < 0) {
					    	    throw new IllegalArgumentException();
					    	}
					    	if (base == 0) {
					    	    throw new ArithmeticException();
					    	}	
				    	
					    System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
					    	if (height < 0) {
					    	    throw new IllegalArgumentException();
					    	}
					    	if (height == 0) {
					    	    throw new ArithmeticException();
					    	}	
					    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Luas Trapesium: %.1f\n",area());
				    	
				    	System.out.printf("Keliling Trapesium: %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka tidak boleh nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}

class circleFormula{
	double radius,phi=(double)22/7;
	double area(){
		return phi * radius * radius;
	}
	double perimeter(){
		return 2.0 * phi * radius;
	}
}
		class circle extends circleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double radius;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Panjang jari-jari Lingkaran (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
		
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    
				    	System.out.printf("Luas Lingkaran: %.3f\n",area());
				    	
				    	System.out.printf("Keliling Lingkaran: %.3f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka tidak boleh nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class rhombusFormula{
	double diagonal1,diagonal2;
	double area(){
		return 1/2*diagonal1*diagonal2;
	}
	double circumference(){
		return Math.sqrt(((1/2*diagonal1)*(1/2*diagonal1))+((1/2*diagonal2)*(1/2*diagonal2)));
	}
}
		class rhombus extends rhombusFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double diagonal1,diagonal2;
			    String choice;
			    do{
				    try{
				    	System.out.println("Masukan Panjang Diagonal 1 (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	diagonal1=input.nextDouble();
				    	this.diagonal1 = diagonal1;
				    	if (diagonal1 < 0) {
				    	    throw new IllegalArgumentException();
				    	}if (diagonal1 == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println("Masukan Panjang Diagonal 2 (Format Angka Bilangan Desimal ex: 1.0): ");
				    	System.out.print(">> ");
				    	diagonal2=input.nextDouble();
				    	this.diagonal2= diagonal2;
				    	if (diagonal2 < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (diagonal2 == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("Luas Belah Ketupat: %.3f\n",area());
				    	
				    	System.out.printf("Keliling Belah Ketupat: %.3f\n",circumference());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Inputan Anda Salah");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Angka tidak boleh nol");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Angka tidak boleh negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Hanya boleh memasukkan huruf 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}
