import java.util.*;
	public class OOPMatriks extends theMatrix{
		Scanner input=new Scanner(System.in);
		int choiced;
		void MainMenu(){
		while(true){
			int choice;
			System.out.println("\t\t   MATRIKS");
			System.out.println("===========================================");
			System.out.println("1. Pertambahan");
			System.out.println("2. Pengurangan");
			System.out.println("3. Perkalian");
			System.out.println("0. Keluar");
			System.out.println("===========================================");	
			System.out.print("Masukkan Pilihan Anda : ");
			do{
				try{
					choice=input.nextInt();
					if(choice!= 1 && choice!= 2 && choice!= 3  && choice!= 0 ){
						System.out.println("Masukkan pilihan Yang Tersedia");
						continue;
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan pilihan Yang Tersedia");
					input.nextLine();
					continue;
				}
			}while(true);
			if(choice== 0){
				System.exit(0);
			}
			switch(choice){
				case 1:
					addition();
					break;
				case 2:
					reduction();
					break;
				case 3:
					multiplication();
					break;	
			}	
		}
	}
	
	void addition(){
		int row,colom;
		do{
			System.out.println("\t\t  Perkalian");
			System.out.println("===========================================");
			do{	
				try{				
					System.out.print("Masukkan Jumlah row Matriks : ");
					row=input.nextInt();
						if (row <1){
							throw new InputMismatchException();
						}
					System.out.print("Masukkan Jumlah colom Matriks : ");
					colom=input.nextInt();
						if (colom <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan Angka Lebih Besar dari 0");
					input.nextLine();
					continue;
				}
			}while(true);			
			double[][]matriks1= new double[row][colom];
			double[][]matriks2= new double[row][colom];
			double[][]result=new double[row][colom];
			System.out.println("\t\t MATRIKS A");
			do{
				try{
					for(int indexrow=0; indexrow<row;indexrow++){
						for(int indexcolom=0; indexcolom<colom;indexcolom++){
							System.out.print("Masukkan Nilai row "+(indexrow+1)+" dan colom "+(indexcolom+1)+" : ");
							matriks1[indexrow][indexcolom]=input.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan Bilangan Real");
					input.nextLine();
					continue;
				}	
			}while(true);
			System.out.println();
			for(int indexrow=0; indexrow<row;indexrow++){
				for(int indexcolom=0; indexcolom<colom;indexcolom++){
					System.out.printf("%.2f  ",matriks1[indexrow][indexcolom]);
				}System.out.println();
			}
			System.out.println();
			System.out.println("\t\t MATRIKS B");
			do{
				try{
					for(int indexrow=0; indexrow<row;indexrow++){
						for(int indexcolom=0; indexcolom<colom;indexcolom++){
							System.out.println("Masukkan Nilai row"+(indexrow+1)+" dan colom "+(indexcolom+1));
							System.out.print(">> ");
							matriks2[indexrow][indexcolom]=input.nextDouble();
							result[indexrow][indexcolom]=matriks1[indexrow][indexcolom] + matriks2[indexrow][indexcolom];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan Bilangan Real");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int indexrow=0; indexrow<row;indexrow++){
				for(int indexcolom=0; indexcolom<colom;indexcolom++){
					
					System.out.printf("%.2f  ",matriks2[indexrow][indexcolom]);
				}System.out.println();
			}			
			System.out.println("\n");
			System.out.println("Hasil dari Penjumlahan Kedua Matriks adalah : ");
			System.out.println();
			for(int indexrow=0; indexrow<row;indexrow++){
				for(int indexcolom=0; indexcolom<colom;indexcolom++){
					
					System.out.printf("%.2f  ",result[indexrow][indexcolom]);
				}System.out.println();
			}
			if(row != colom){
				System.out.println("Matriks Tersebut tidak Memiliki Determinan");
			}	
			else{
				
				if(row==2 && colom==2){
					if(((result[0][0]*result[1][1])-(result[0][1]*result[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((result[0][0]*result[1][1])-(result[0][1]*result[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan Matriks Pencerminan");
					}
				}
				else if(row==3 && colom==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan Matriks Pencerminan");
					}
				}
			}	
			System.out.println();
			input.nextInt();
			do{
				System.out.println("Tekan 1 Untuk Ulangi dan 0 Untuk Keluar");
				choiced=input.nextInt();
				if (choiced==0){
					 break;
				 }
				 else  if (choiced==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			 if (choiced==0){
				 break;
			 }
			 else  if (choiced==1){
				 continue;
			 }
		}while(true);		
	}
	
	void reduction(){
		int row,colom;
		do{
			System.out.println("\t\t  Pengurangan");
			System.out.println("===========================================");	
			do{	
				try{
					
					System.out.print("Masukkan Jumlah row Matriks : ");
					row=input.nextInt();
						if (row <1){
							throw new InputMismatchException();
						}
					System.out.print("Masukkan Jumlah colom Matriks : ");
					colom=input.nextInt();
						if (colom <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan Angka Lebih Besar dari 0");
					input.nextLine();
					continue;
				}
			}while(true);	
			double[][]matriks1= new double[row][colom];
			double[][]matriks2= new double[row][colom];
			double[][]result=new double[row][colom];
			System.out.println("\t\t MATRIKS A");
			do{
				try{
					for(int indexrow=0; indexrow<row;indexrow++){
						for(int indexcolom=0; indexcolom<colom;indexcolom++){
							System.out.println("Masukkan Nilai row "+(indexrow+1)+" dan colom "+(indexcolom+1));
							System.out.print(">> ");
							matriks1[indexrow][indexcolom]=input.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan Bilangan Real");
					input.nextLine();
					continue;
				}	
			}while(true);			
			System.out.println();
			for(int indexrow=0; indexrow<row;indexrow++){
				for(int indexcolom=0; indexcolom<colom;indexcolom++){
					
					System.out.printf("%.2f  ",matriks1[indexrow][indexcolom]);
				}System.out.println();
			}	
			System.out.println();		
			System.out.println("\t\t MATRIKS B");
			do{
				try{
					for(int indexrow=0; indexrow<row;indexrow++){
						for(int indexcolom=0; indexcolom<colom;indexcolom++){
							System.out.println("Masukkan Nilai row "+(indexrow+1)+" dan colom "+(indexcolom+1));
							System.out.print(">> ");
							matriks2[indexrow][indexcolom]=input.nextDouble();
							result[indexrow][indexcolom]= matriks1[indexrow][indexcolom] - matriks2[indexrow][indexcolom];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan Bilangan Real");
					input.nextLine();
					continue;
				}	
			}while(true);		
			System.out.println();
			for(int indexrow=0; indexrow<row;indexrow++){
				for(int indexcolom=0; indexcolom<colom;indexcolom++){
					
					System.out.printf("%.2f  ",matriks2[indexrow][indexcolom]);
				}System.out.println();
			}		
			System.out.println("\n");
			System.out.println("Hasil Pengurangan Kedua Matriks Tersebut Adalah : ");
			System.out.println();	
			for(int indexrow=0; indexrow<row;indexrow++){
				for(int indexcolom=0; indexcolom<colom;indexcolom++){
					
					System.out.printf("%.2f  ",result[indexrow][indexcolom]);
				}System.out.println();
			}
			if(row != colom){
				System.out.println("Matriks Tersebut tidak Memiliki Determinan");
			}	
			else{
				
				if(row==2 && colom==2){
					if(((result[0][0]*result[1][1])-(result[0][1]*result[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((result[0][0]*result[1][1])-(result[0][1]*result[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan Matriks Pencerminan");
					}
				}
				else if(row==3 && colom==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan Matriks Pencerminan");
					}
				}
			}
			System.out.println();
			input.nextLine();
			do{
				System.out.println("Tekan 1 Untuk Ulangi dan 0 Untuk Keluar");
				choiced=input.nextInt();
				if (choiced==0){
					 break;
				 }
				 else  if (choiced==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			
				 if (choiced==0){
					 break;
				 }
				 else  if (choiced==1){
					 continue;
				 }		
		}while(true);	
	}
	
	void multiplication(){	
		while(true){
			 int matriksMultiTotal=0,choiced=0,row1,colom1,row2,colom2;
		 do{
			 do{
				 try{
					 System.out.print("Jumlah row Matriks A: ");
					 row1=input.nextInt();
					 System.out.print("Jumlah colom Matriks A: ");
					 colom1=input.nextInt();
					 System.out.print("Jumlah row Matriks B: ");
					 row2=input.nextInt();
					 System.out.print("Jumlah colom Matriks B: ");
					 colom2=input.nextInt();
					 break;
				 }catch(InputMismatchException e){
					 System.out.println("Masukkan Bilangan Bulat");
					 input.nextLine();
					 continue;
				 }
			 }while(true); 
					if (colom1 != row2) {
						System.out.println("Matriks Tidak Dapat Dikalikan");					
						do{
							try{
								System.out.print("Tekan 1 untuk Ulangi dan 0 untuk Keluar : ");
								choiced=input.nextInt();
								if (choiced==0)break;
								if (choiced==1)break;
							}catch(InputMismatchException e){
								System.out.println("Masukkan choiced Yang Tersedia");
								input.nextLine();
								continue;
							}
						}while((choiced!=0)||(choiced!=1));
					}			
					if (choiced==0)break;
					if (choiced==1)continue;
		 }while(colom1!=row2);
			 if (choiced==1)break;
			 System.out.println("\n"); 
		int[][]matriks=new int[row1][colom1];
		do{
			System.out.println("Matrix A: ");
			for(int indexrow=0;indexrow<row1;indexrow++){
				for(int indexcolom=0;indexcolom<colom1;indexcolom++){
					System.out.print("row "+(indexrow+1)+" colom "+(indexcolom+1)+" : ");
					try{
						matriks[indexrow][indexcolom]=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan Bilangan Bulat");
						input.nextLine();
						indexcolom--;
						continue;
					}	
				}
			}
			break;
		}while(true);		
		System.out.println();	
		for(int indexrow=0;indexrow<row1;indexrow++){
			for(int indexcolom=0;indexcolom<colom1;indexcolom++){
				System.out.printf("%4d ", matriks[indexrow][indexcolom]);
			}System.out.println();
		}System.out.println();
		int[][]matriks2=new int[row2][colom2];
		do{
			System.out.println("Matrix B: ");
			for(int indexrow=0;indexrow<row2;indexrow++){
				for(int indexcolom=0;indexcolom<colom2;indexcolom++){
					System.out.print("row "+(indexrow+1)+" colom "+(indexcolom+1)+" : ");
					try{
						matriks2[indexrow][indexcolom]=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan Bilangan Bulat");
						input.nextLine();
						indexcolom--;
						continue;
					}
				}
			}
			break;
		}while(true);	
		System.out.println();	
		for(int indexrow=0;indexrow<row2;indexrow++){
			for(int indexcolom=0;indexcolom<colom2;indexcolom++){
				System.out.printf("%4d ", matriks2[indexrow][indexcolom]);
			}System.out.println();
		}	
		int[][]matriks3=new int[row1][colom2];
		for(int indexrow=0;indexrow<row1;indexrow++){
			for(int indexcolom=0;indexcolom<colom2;indexcolom++){
				matriksMultiTotal=0;
				for(int multiplicationindex=0;multiplicationindex<colom1;multiplicationindex++){
					matriksMultiTotal+=matriks[indexrow][multiplicationindex]*matriks2[multiplicationindex][indexcolom];
				}
				matriks3[indexrow][indexcolom]=matriksMultiTotal;
					}
				}System.out.println();		
		System.out.println("multiplication dari Matriks "+row1+"x"+colom1+" X "+row2+"x"+colom2+" Akan Membuat Matriks "+row1+"x"+colom2);
		System.out.println("result Dari multiplication Kedua Matriks adalah : ");
		for(int indexrow=0;indexrow<row1;indexrow++){
			for(int indexcolom=0;indexcolom<colom2;indexcolom++){
				System.out.printf("%4d ", matriks3[indexrow][indexcolom]);
			}
			System.out.println();
		}		
		System.out.println("\n");		
		if(row1 != colom2){
			System.out.println("Matriks Tersebut tidak Memiliki Determinan");
		}	
		else{
			if(row1==2 && colom2==2){
				if(((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0]))!=0){
					System.out.println("Determinan dari matriks tersebut adalah "+((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0])));
				}
				else{
					System.out.println("Matriks tersebut merupakan Matriks Pencerminan");
				}
			}
			else if(row1==3 && colom2==3){
				if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
						-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
						+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
					
					System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
				}
				else{
					System.out.println("Matriks tersebut merupakan Matriks Pencerminan");
				}
			}
		}		
			do{
				try{
					System.out.print("Tekan 1 untuk Ulangi dan 0 untuk Keluar : ");
					choiced=input.nextInt();
					if (choiced==0)break;
					if (choiced==1)break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan choiced Yang Tersedia");
					input.nextLine();
					continue;
				}		
			}while((choiced<0)||(choiced>1));
			if (choiced==0)break;
			if(choiced==1)continue;
			System.out.println();
		 }
	}
	
	public static void main(String[] args){
		OOPMatriks call = new OOPMatriks();
		try{
			call.MainMenu();
		}catch(NoSuchElementException e){
			System.out.println("\n");
			System.out.println("You pressed Ctrl + Z");
			System.out.println("Program terminated");
		}		
		//nothing else to see here
    }	
}
class theMatrix{
	double matrix1,matrix2,multiply;
		double add(){
			return matrix1+matrix2;
	}
		double substract(){
			return matrix1-matrix2;
		}
		double multiply(){
			return multiply;
		}
}