import java.util.*;

public class calculator {
	private double value1;
	private double value2;
	private double result;

	public void setValue1(double n){
	value1 = n;
	}

	public double getValue1(){
		return value1;
	}

	public void setValue2(double n){
	value2 = n;
	}

	public double getValue2(){
		return value2;
	}

	public double getresult(){
		return result;
	}
	
	public void additionresult(){
		result = value1 + value2;
	}

	public void reductionresult(){
		result = value1 - value2;
	}
	
	public void multiplyresult(){
		result = value1*value2;
	}
	
	public void divideresult(){
		result = value1/value2;
	}
	
	public void rootresult(){
		result = Math.sqrt(value1);
	}
	
	public void powerresult(){
		result = Math.pow(value1, value2);
	}

	}

	class kalkulatormain{
		
		public static void main (String [] args){
			calculator object = new calculator();
			Scanner input = new Scanner(System.in);
			boolean kontinu = false;
			int choice;
			
			do{
				try{
				System.out.println("Kalkulator");
				System.out.println("1. Penjumlahan");
				System.out.println("2. Pengurangan");
				System.out.println("3. Perkalian");
				System.out.println("4. Pembagian");
				System.out.println("5. Akar");
				System.out.println("6. Pangkat");
				System.out.println("7. Exit");
				
				System.out.println("");
				System.out.print("Masukan Pilihan Anda : ");
				choice = input.nextInt();
				System.out.println("");
				
				
				if(choice==1){
					try{
					System.out.print("Masukan Bilangan Pertama : ");
					double val1=input.nextDouble();
					object.setValue1(val1);
					System.out.println();
					
					System.out.print("Masukan Bilangan Kedua : ");
					double val2=input.nextDouble();
					object.setValue2(val2);
					System.out.println();
					
					object.additionresult();
					System.out.println(val1+" + "+val2+" = "+object.getresult());
					System.out.println();
					}
					
					catch(InputMismatchException e){
					System.out.println("Inputan Anda Salah");
					System.out.println();
					input.nextLine();
					continue;	
					}
						
					catch (NoSuchElementException e){
					System.out.println("Don't Press Ctrl+Z");
					}
					
				}
				
				else if (choice==2){
					try{
					System.out.print("Masukan Bilangan Pertama : ");
					double val1=input.nextDouble();
					object.setValue1(val1);
					System.out.println();
						
					System.out.print("Masukan Bilangan Kedua : ");
					double val2=input.nextDouble();
					object.setValue2(val2);
					System.out.println();
					
						
					
					object.reductionresult();
					System.out.println(val1+" - "+val2+" = "+object.getresult());	
					System.out.println();
					}
					
					catch(InputMismatchException e){
					System.out.println("Inputan Anda Salah");
					System.out.println();
					input.nextLine();
					continue;	
					}
						
					catch (NoSuchElementException e){
					System.out.println("Don't Press Ctrl+Z");
					}
					
				}
				
				else if(choice==3){
					try{
						System.out.print("Masukan Bilangan Pertama : ");
						double val1=input.nextDouble();
						object.setValue1(val1);
						System.out.println();
						
						System.out.print("Masukan Bilangan Kedua : ");
						double val2=input.nextDouble();
						object.setValue2(val2);
						System.out.println();
						
						object.multiplyresult();
						System.out.println(val1+" x "+val2+" = "+object.getresult());	
						System.out.println();
						
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
						
					}
				
				else if(choice==4){
					try{
						System.out.print("Masukan Bilangan Pertama : ");
						double val1=input.nextDouble();
						object.setValue1(val1);
						System.out.println();
						
						System.out.print("Masukan Bilangan Kedua : ");
						double val2=input.nextDouble();
						object.setValue2(val2);
						System.out.println();
						
						object.divideresult();
						System.out.println(val1+" : "+val2+" = "+object.getresult());	
						System.out.println();
						
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
					}
				
				else if(choice==5){
					try{
						System.out.println("Masukan Bilangan yang akan di akarkan : ");
						double val1=input.nextDouble();
						object.setValue1(val1);
						System.out.println();
						
						object.rootresult();
						System.out.println("Akar dari "+val1+" = "+object.getresult());	
						System.out.println();
					
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
					}
				
				else if(choice==6){
					try{
						System.out.print("Masukan Bilangan : ");
						double val1=input.nextDouble();
						object.setValue1(val1);
						System.out.println();
						
						System.out.print("Masukan Pangkat : ");
						double val2=input.nextDouble();
						object.setValue2(val2);
						System.out.println();
						
						object.powerresult();
						System.out.println(val1+" Pangkat "+val2+" = "+object.getresult());	
						System.out.println();
					
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
					}
				
				else if(choice==7){
					System.out.println("Terima Kasih");
					break;
					
				}
				
				else{
					System.out.println("Inputan anda salah");
					System.out.println();
				}
				}
				
				catch(InputMismatchException e){
				System.out.println("Inputan Anda Salah");
				System.out.println();
				input.nextLine();
				continue;	
				}
				
				catch (NoSuchElementException e){
					System.out.println("Don't Press Ctrl+Z");
				}

				}while(kontinu=true);

		}

	}